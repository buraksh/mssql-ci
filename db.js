const Sequelize = require('sequelize')
const dotenv = require('dotenv')
dotenv.config()

const sequelize = new Sequelize(process.env.DB_NAME, 'sa', process.env.SA_PASSWORD, {
  host: process.env.MSSQL_HOST,
  // port: 1433,
  dialect: 'mssql',
})

async function connect() {
  try {
    await sequelize.authenticate();

    console.log('Sequelize properly connected', {
      host: process.env.MSSQL_HOST,
      password: process.env.SA_PASSWORD,
    });

  } catch (error) {
    console.error('An error occured', {
      error
    });

    process.exit(1);
  }
}

connect();
