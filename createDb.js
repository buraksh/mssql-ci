const { Connection, Request } = require('tedious')
const dotenv = require('dotenv')
dotenv.config()

const config = {
  server: process.env.MSSQL_HOST,
  authentication: {
    type: 'default',
    options: {
      userName: 'sa',
      password: process.env.SA_PASSWORD,
    },
  },
}

const connection = new Connection(config)

new Promise((resolve, reject) => {
  connection.on('connect', function(err) {
    connection.execSql(
      new Request('CREATE DATABASE ' + process.env.DB_NAME, err => {
        if (err) return reject(err)
        resolve()
      })
    )
  })
})
  .then(() => {
    console.log('Successfully created database', {
      host: process.env.MSSQL_HOST,
      password: process.env.SA_PASSWORD,
    });
    process.exit()
  })
  .catch(console.error)
